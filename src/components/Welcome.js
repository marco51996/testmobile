import React from 'react';
import { SafeAreaView } from 'react-native';
import * as Animatable from 'react-native-animatable';
import styles from './Styles';

export default () => {
    return (
        <SafeAreaView style={styles.screenWhite}>
            <Animatable.Text style={{
                fontWeight: 'bold',
                fontSize: 32,
            }}
                animation="fadeInRightBig" iterationCount={1} duration={1000}
                useNativeDriver>Bienvenido</Animatable.Text>
        </SafeAreaView>
    )
}