import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    containerHeader: {
        flexDirection: 'row',
        backgroundColor: '#fff'
    },
    titleHeader: {
        fontSize: 20,
        marginVertical: 10,
        fontWeight: 'bold',
    },
    boxHeader: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5
    },
    screenWhite: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        position: 'relative',
        elevation: 3,
        alignItems: 'center'
    },
    card: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginHorizontal: 10,
        marginVertical: 5,
        borderRadius: 10,
    },
    shadow: {
        elevation: 2,
        shadowColor: "#000",
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        shadowOffset: { width: 0, height: 1 },
    },
    img: {
        width: '28%',
        height: 100,
        margin: 10,
    }
})