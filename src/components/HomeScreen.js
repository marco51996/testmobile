import React, { useEffect, useState, memo } from 'react';
import { Text, SafeAreaView, TouchableOpacity, FlatList } from 'react-native';
import { getListDogs } from '../api/DogAPI';
import Welcome from './Welcome';
import styles from './Styles';

const App = (props) => {

    const [listDogs, setListDogs] = useState();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        let isMounted = true;
        getListDogs().then((data) => {
            if (isMounted) { setListDogs(data) }
        });
        setTimeout(() => { setIsLoading(false) }, 1500);
        return () => { isMounted = false };
    });

    return (
        <SafeAreaView style={styles.container}>
            {isLoading && <Welcome />}
            <FlatList
                data={listDogs}
                renderItem={item => {
                    return (
                        <TouchableOpacity style={[styles.card, styles.shadow]}
                            onPress={() => { props.navigation.navigate('PicturesDogsScreen', { breed: item.item }) }}>
                            <Text style={{ fontSize: 20 }}>{item.item[0].toUpperCase() + item.item.slice(1)}</Text>
                        </TouchableOpacity>
                    )
                }}
                keyExtractor={item => item} />
        </SafeAreaView>
    );
};

export default memo(App);




