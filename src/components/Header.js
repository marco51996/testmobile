import React from 'react';
import { SafeAreaView, Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
import styles from './Styles';

export default () => {
    return (
        <SafeAreaView style={[styles.containerHeader,
        { flexDirection: 'row' }, styles.shadow]}>
            <Animatable.View
                animation="fadeIn" iterationCount={1} duration={1500} useNativeDriver
                style={styles.boxHeader}>
                <Text style={styles.titleHeader}>Razas de perros</Text>
            </Animatable.View>
        </SafeAreaView>
    )
}