import React, { memo, useEffect, useState } from 'react';
import { Image, FlatList, View } from 'react-native';
import { getImagesDogs } from '../api/DogAPI';
import styles from './Styles';

const PicturesDogs = (props) => {

    const [images, setImages] = useState(false);

    useEffect(() => {
        let isMounted = true;
        getImagesDogs(props.route.params.breed).then(data => {
            if (isMounted) setImages(data);
        })
        return () => { isMounted = false };
    })

    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ flexDirection: 'column' }}
                numColumns={3}
                data={images}
                initialNumToRender={8}
                renderItem={item => {
                    return (<Image source={{ uri: item.item }} style={styles.img} />)
                }}
                keyExtractor={item => item} />
        </View>
    )
}

export default memo(PicturesDogs);
