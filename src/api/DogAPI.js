export const getListDogs = async () => {
    const response = await fetch('https://dog.ceo/api/breeds/list');
    const result = await response.json();
    return result.message;
};

export const getImagesDogs = async (breedName) => {
    const response = await fetch('https://dog.ceo/api/breed/' + `${breedName}` + '/images');
    const result = await response.json();
    return result.message;
};


