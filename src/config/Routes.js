import Home from '../components/HomeScreen'
import PicturesDogs from '../components/PicturesDogs'

export default Routes = {
    HomeScreen: Home,
    PicturesDogsScreen: PicturesDogs,
}
