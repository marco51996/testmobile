import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import Header from './src/components/Header';
import Routes from './src/config/Routes';

const AppStacks = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <AppStacks.Navigator
        screenOptions={{
          headerTitleAlign: 'center',
          cardStyleInterpolator:
            CardStyleInterpolators.forHorizontalIOS,
        }}>
        <AppStacks.Screen
          name="HomeScreen" component={Routes.HomeScreen}
          options={{ header: props => <Header {...props} /> }}
        />
        <AppStacks.Screen
          name="PicturesDogsScreen"
          component={Routes.PicturesDogsScreen}
          options={{ title: 'Fotos' }}
        />
      </AppStacks.Navigator>
    </NavigationContainer>
  );
};

export default App;
