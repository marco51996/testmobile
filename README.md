# Test Mobile

###### Versiones utilizadas

* Watchman 4.9.0
* Homebrew 2.2.15 (macOS)
* React Navigation 5

##### 1. Dependencias del sistema

* Watchman: `brew install watchman`

##### 2. Iniciar proyecto

* cd testmobile
* npx react-native start
* npx react-native run-android

